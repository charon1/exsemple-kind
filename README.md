# คู่มือการใช้งาน Kubernetes, Docker, และ Django

## บทนำ

คู่มือนี้เป็นการสร้างและใช้งาน Kubernetes, Docker, และ Django บนระบบ Ubuntu 20.04 เพื่อพัฒนาแอปพลิเคชันโดยใช้เทคโนโลยีเหล่านี้ในการสร้างสภาพแวดล้อมและตัวแปรแอปพลิเคชัน ตั้งแต่ขั้นตอนการติดตั้งระบบพื้นฐาน, สร้าง Docker Image ของ nginx-custom และ Django, สร้าง Kubernetes Cluster, การใช้งาน Persistent Volume (PV) และ Persistent Volume Claim (PVC), การกำหนดค่าด้วย Configmap และ Secret, ประยุกต์ใช้งานระบบใน Kubernetes Cluster และสรุปและตอบคำถาม

## ขั้นตอนการติดตั้งและสร้างสภาพแวดล้อมพื้นฐาน

1. ติดตั้ง Docker:
   - ใช้คำสั่งนี้เพื่อติดตั้ง Docker บน Ubuntu:
     ```
     sudo apt update
     sudo apt install docker.io
     ```
   - เริ่มต้น Docker service:
     ```
     sudo systemctl start docker
     sudo systemctl enable docker
     ```

2. ติดตั้ง kind (Kubernetes IN Docker):
   - ใช้คำสั่งนี้เพื่อดาวน์โหลดและติดตั้ง kind:
     ```
     curl -Lo kind https://kind.sigs.k8s.io/dl/v0.11.1/kind-linux-amd64
     chmod +x kind
     sudo mv kind /usr/local/bin
     ```

3. ติดตั้ง Django:
   - สร้าง Virtual Environment และเปิดใช้งาน:
     ```
     python3 -m venv venv
     source venv/bin/activate
     ```
   - ติดตั้ง Django ด้วย pip:
     ```
     pip install django
     ```

4. สร้าง Kubernetes Cluster ด้วย kind:
   - ใช้คำสั่งนี้เพื่อสร้าง Kubernetes Cluster ชื่อ "my-cluster":
     ```
     kind create cluster --name my-cluster --wait 1m
     ```
   - คำสั่งข้างต้นจะสร้าง Kubernetes Cluster ในเครื่องที่ใช้พัฒนา เมื่อ Cluster สร้างเสร็จให้ตรวจสอบให้แน่ใจว่า Cluster ทำงานอยู่ก่อนที่จะดำเนินการขั้นตอนถัดไป

## ติดตั้ง Kubectl

Kubectl เป็นเครื่องมือที่มีประโยชน์ในการวิเคราะห์และตรวจสอบสคริปต์ shell หากคุณต้องการติดตั้ง ShellCheck บนระบบของคุณให้ดำเนินการดังนี้:

```sh
curl -Lo shellcheck-v0.7.1.linux.aarch64.tar.xz https://github.com/koalaman/shellcheck/releases/download/v0.7.1/shellcheck-v0.7.1.linux.aarch64.tar.xz
tar -xvf shellcheck-v0.7.1.linux.aarch64.tar.xz
chmod +x shellcheck-v0.7.1/shellcheck
sudo mv shellcheck-v0.7.1/shellcheck /usr/local/bin/
rm -rf shellcheck*xrx
sudo chown -R coder:coder /home/coder/.local/share/code-server \
&& curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl \
&& chmod +x kubectl && sudo mv kubectl /usr/local/bin/

```

เมื่อทำเสร็จสิ้นขั้นตอนข้างต้น คุณจะได้รับการติดตั้ง ShellCheck บนระบบของคุณ คุณสามารถใช้ ShellCheck เพื่อตรวจสอบสคริปต์ shell ของคุณเพื่อค้นหาข้อผิดพลาดที่เป็นไปได้และปรับปรุงคุณภาพของสคริปต์

```
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.13.7/config/manifests/metallb-native.yaml
kubectl apply -f https://kind.sigs.k8s.io/examples/loadbalancer/metallb-config.yaml
```
a. สร้าง Deployment สำหรับ Django

สร้างไฟล์ `django-deployment.yaml`:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: django-deployment
spec:
  replicas: 2
  selector:
    matchLabels:
      app: django
  template:
    metadata:
      labels:
        app: django
    spec:
      containers:
      - name: django
        image: kran13200/django:latest
        ports:
        - containerPort: 8000
```

สร้าง Deployment:

```bash
kubectl apply -f django-deployment.yaml -n default
```

### b. สร้าง Service LoadBalancer สำหรับ Django

สร้างไฟล์ `django-service.yaml`:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: django-service
spec:
  type: LoadBalancer
  selector:
    app: django
  ports:
    - protocol: TCP
      port: 8000
      targetPort: 8000
```

สร้าง Service:

```bash
cd kubernetes 
kubectl apply -f django-service.yaml -n default
kubectl get pod -n default
kubectl port-forward services/django-service --address 0.0.0.0 8000:8000 -n default 
```

หลังจากนั้น Service LoadBalancer จะสร้างไอพีสาธารณะให้สามารถเข้าถึง Django app ได้ผ่าน Port 8000.
https://jupyter-dev.kg.sec.or.th/user/<username>/proxy/8000/
